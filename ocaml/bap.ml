(* bap.ml                                                                  *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility

let str_to_arch = function
  | "x86" -> Libbfd.Bfd_arch_i386
  | "arm" -> Libbfd.Bfd_arch_arm
  | _ -> failwith "Unsupported architecture"

let exception_wrapper f =
  try
    f()
  with err ->
    let bt = Printexc.get_backtrace() in
      failwith(Printexc.to_string(err) ^ "\n" ^ bt)

let byte_sequence_to_bap arch bytes addr = 
  exception_wrapper(fun () -> List.concat(Asmir.byte_sequence_to_bap (Array.of_list(BatString.explode bytes)) (str_to_arch arch) addr))

let _ = Callback.register "byte_sequence_to_bap" byte_sequence_to_bap

let bap_to_string ast = 
  let buf = Buffer.create 1000 in
  let out = Buffer.add_substring buf
    and spaces _ = Buffer.add_char buf ' '
    and newline _ = Buffer.add_char buf '\n' in
  let ft = Format.formatter_of_buffer buf in
  let () = Format.pp_set_all_formatter_output_functions ft ~out ~flush:ignore ~spaces ~newline in
  let strpp = new Pp.pp ft in
    Buffer.add_char buf ' '; (* FIXME: hack to get around space indenting with linewraps! *)
    strpp#ast_program ast;
    let s = Buffer.contents buf in
      Buffer.reset buf;
      s

let _ = Callback.register "bap_to_string" (fun p -> exception_wrapper(fun () -> bap_to_string p))

let rec apply_cmd cmd_opts (prog_term, model, wp, foralls) =
  let cmd_check cmd_type cmd = 
    if Array.length(cmd) > 0 then
      cmd_type = cmd.(0)
    else
      failwith "Commands need to specify a type"
  in
  let cmd_type = match cmd_opts with 
    | [] -> failwith "Need to provide at least one command" 
    | c ::cs -> 
        if Array.length(c) > 0 then 
          c.(0) 
        else 
          failwith "Commands need to specify a type" 
  in
  let common_cmds = BatList.take_while (cmd_check cmd_type) cmd_opts in
  let remaining_cmds = BatList.drop_while (cmd_check cmd_type) cmd_opts in
  let new_prog_term, model, wp, foralls =
    if cmd_type = "iltrans" then
      (Iltrans.apply_cmd common_cmds prog_term, model, wp, foralls)
    else if cmd_type = "topredicate" then
      let ast_term = match prog_term with
        | Iltrans.Ast ast_term -> ast_term
        | Iltrans.Ssa _ -> failwith "Need to convert static single assignment (SSA) term into an abstract syntax tree (AST) term"
        | Iltrans.AstCfg _ -> failwith "Need to convert control-flow graph (CFG) term into an abstract syntax tree (AST) term"
      in
      let term, model, wp, foralls = Topredicate.apply_cmd common_cmds ast_term in
        (Iltrans.Ast term, model, wp, foralls)
    else if cmd_type = "codegen" then
      let astcfg_term = match prog_term with
        | Iltrans.Ast ast_term -> Cfg_ast.of_prog ast_term
        | Iltrans.Ssa _ -> failwith "Need to convert static single assignment (SSA) term into an abstract syntax tree (AST) or AST control-flow graph (ASTCFG) term"
        | Iltrans.AstCfg astcfg_term -> astcfg_term
      in
        (Iltrans.AstCfg (Codegen.apply_cmd common_cmds astcfg_term), model, wp, foralls)
    else
      failwith (cmd_type ^ " is not a recognised command type")
  in
    if remaining_cmds = [] then
      (new_prog_term, model, wp, foralls)
    else
      apply_cmd remaining_cmds (new_prog_term, model, wp, foralls)

let bap_to_python = function
  | Iltrans.Ast(ast) ->
      Bap_ast.to_python(ast)
  | Iltrans.AstCfg(cfg) ->
      Bap_cfg.to_python(cfg)
  | Iltrans.Ssa(ssa) ->
      Bap_ssa.to_python(ssa)

let _ = Callback.register "apply_cmd" (fun cmd_opts ast_term -> 
  let filtered_cmds = List.filter (fun cmd -> Array.length cmd >= 2 && cmd.(0) <> "control") cmd_opts in
  let term, model, wp, foralls = if List.length filtered_cmds = 0 then (Iltrans.Ast ast_term, None, None, None) else exception_wrapper(fun () -> apply_cmd filtered_cmds (Iltrans.Ast ast_term, None, None, None)) in
    flush_all();
    if List.mem [| "control"; "-return" |] cmd_opts then (
      let result_list = ref [ (PyString "term", bap_to_python term) ] in
        if List.length !Bap_analysis.result > 0 then result_list := !Bap_analysis.result @ !result_list;
        if BatOption.is_some model then result_list := (PyString "model", Bap_stp.to_python(BatOption.get model)) :: !result_list;
        if BatOption.is_some wp then result_list := (PyString "wp", PyDict [ (PyString "pre", Bap_ast.exp_to_python(BatOption.get wp)); (PyString "foralls", Bap_ast.vars_to_python(BatOption.get foralls)) ]) :: !result_list;
        PyDict !result_list
    ) else
        PyNone
)
