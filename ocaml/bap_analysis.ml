(* bap_analysis.ml                                                         *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility

let result = ref []

let location_to_python = function
  | Depgraphs.UseDef_AST.LocationType.Undefined ->
      PyString "undefined"
  | Depgraphs.UseDef_AST.LocationType.Loc(bb, i) ->
      PyDict [ (PyString "loc", PyTuple [| PyString(Cfg.bbid_to_string(Cfg.AST.G.V.label bb)); Bap_ast.int_to_python i |]) ]

let var_to_python var = PyString((Var.name var) ^ "_" ^ (string_of_int(Var.hash var)))

let usedef usedef =
  let module UD = Depgraphs.UseDef_AST in
  let module VM = Var.VarMap in
  let h, _ = UD.usedef usedef in
    result := (PyString "usedef", PyDict(Hashtbl.fold
      (fun (bb, i) varmap py_usedefs ->
        (PyTuple [| PyString(Cfg.bbid_to_string(Cfg.AST.G.V.label bb)); Bap_ast.int_to_python i |],
         PyDict(VM.fold
          (fun v defset py_vardefs ->
            let py_defs = List.map location_to_python (UD.LS.elements defset) in
              (var_to_python v, PyList py_defs) :: py_vardefs
          )
          varmap
          []
         )
        ) :: py_usedefs
      )
      h
      []
    )) :: !result

let defuse defuse =
  let module UD = Depgraphs.UseDef_AST in
  let module VM = Var.VarMap in
  let h, _ = UD.defuse defuse in
    result := (PyString "defuse", PyDict(Hashtbl.fold
      (fun (bb, i) varmap py_defuses ->
        (PyTuple [| PyString(Cfg.bbid_to_string(Cfg.AST.G.V.label bb)); Bap_ast.int_to_python i |],
         PyDict(VM.fold
          (fun v defset py_vardefs ->
            let py_defs = List.map location_to_python (UD.LS.elements defset) in
              (var_to_python v, PyList py_defs) :: py_vardefs
          )
          varmap
          []
        )) :: py_defuses
      )
      h
      []
    )) :: !result

let vsa ssa =
  let _, dfout = Vsa.AlmostVSA.DF.worklist_iterate ssa in
  let buf = Buffer.create 100 in
  let string_value ae =
    begin match ae with
    | `Scalar vs -> Vsa.VS.pp (Buffer.add_string buf) vs
    | `Array ae ->
        if ae = Vsa.AllocEnv.top then
          Buffer.add_string buf "AllocEnv.top"
        else
          (Buffer.add_string buf "[";
           Vsa.AllocEnv.fold (fun (r,i) vs a ->
             Buffer.add_string buf ("(" ^ (Pp.var_to_string r) ^ ", " ^ (Int64.to_string i) ^ ") -&gt; ");
             Vsa.VS.pp (Buffer.add_string buf) vs
           ) ae ();
           Buffer.add_string buf "]"
          )
    end;
    let o = Buffer.contents buf in
      Buffer.clear buf;
      o
  in
  let add_vsa_formula bb = 
    let stmts = Cfg.SSA.get_stmts ssa bb in
    let module VARS = Var.VarSet in
    let bb_vars = ref VARS.empty in
    let bb_visitor = 
      object(self) inherit Ssa_visitor.nop
        method visit_avar v = 
          bb_vars := VARS.add v !bb_vars;
          Type.DoChildren

        method visit_rvar v =
          bb_vars := VARS.add v !bb_vars;
          Type.DoChildren
      end
    in
      ignore(Ssa_visitor.stmts_accept bb_visitor stmts);
      let attrs = Var.VarMap.fold 
        (fun v ae l -> 
          Type.StrAttr((Pp.var_to_string v) ^ " :: " ^ (string_value ae)) :: l
        )
        (Var.VarMap.filter (fun v ae -> VARS.mem v !bb_vars) (dfout bb))
        []
      in
        if List.length(attrs) > 0 then
          Ssa.Comment("", attrs) :: stmts
        else
          stmts
  in
    Cfg.SSA.G.fold_vertex
      (fun bb g -> Cfg.SSA.set_stmts g bb (add_vsa_formula bb))
      ssa
      ssa
